import discord
from discord.ext import commands
from forex_python.converter import CurrencyRates
import random
import requests


def setup(bot):
    bot.add_cog(Math(bot))

class Math:
    """Math Cog"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def add(self, ctx, left: int, right: int):
        """Adds two numbers together."""
        await ctx.send(left + right)

    @commands.command()
    async def math(self, ctx, operations: str):
        #operations = str(operations)
        #output = eval(operations)
        await ctx.send(eval(operations))

    @commands.command()
    async def convert(self, ctx, money: str, ct1: str ,ct2: str):
        money = money.replace(",","")
        #oldmoney = oldmoney.replace(",","")
        oldmoney = float(money)
        money = float(money)
        ct1 = ct1.upper()
        ct2 = ct2.upper()
        convertmoney = CurrencyRates()
        money = float(convertmoney.convert(ct1, ct2, money))
        #money = "{:.2f}".format(round(money, 2))
        money = format(money, '.2f')
        oldmoney = format(oldmoney, '.2f')
        #money = "{:,s}".format(float(money))
        #oldmoney = "{:,s}".format(float(oldmoney))
        await ctx.send(str(format(float(oldmoney),",")) + " " + str(ct1) + " to " + str(ct2) + " is " + str(format(float(money),",")))

    @commands.command()
    async def temp(self, ctx, temp: float, unit1: str, nothing: str, unit2: str):
        unit1 = unit1.lower()
        unit2 = unit2.lower()
        if unit1 == "c" and unit2 == "f":
            oldtemp = temp
            temp = 9.0 / 5.0 * float(temp) + 32
            await ctx.send(str(format(oldtemp,",")) + "° Celsius is " + str(format(temp,",")) + "° Fahrenheit")
        elif unit1 =="c" and unit2 == "k":
            oldtemp = temp
            temp = float(temp) + 273.15
            await ctx.send(str(format(oldtemp,",")) + "° Celsius is " + str(format(temp,",")) + " Kelvin")
        elif unit1 == "f" and unit2 == "c":
            oldtemp = temp
            temp = (float(temp) - 32)  / 9.0 * 5.0
            temp = round(temp, 2)
            await ctx.send(str(format(oldtemp,",")) + "° Fahrenheit is " + str(format(temp,",")) + "° Celsius")
        elif unit1 =="f" and unit2 == "k":
            oldtemp = temp
            #itemp = 5 / 9 (float(temp) - 32) + 273
            temp = (float(temp) + 459.67) * 5 / 9
            temp = round(temp,2)
            await ctx.send(str(format(oldtemp,",")) + "° Fahrenheit is " + str(format(temp,",")) + " Kelvin")
        elif unit1 == "k" and unit2 == "c":
            oldtemp = temp
            temp = float(temp) - 273.15
            temp = round(temp,2)
            await ctx.send(str(format(oldtemp,",")) + " Kelvin is " + str(format(temp,",")) + "° Celsius")
        elif unit1 == "k" and unit2 == "f":
            oldtemp = temp
            #temp = 9 / 5 (float(temp) - 273) + 32
            temp = 9 / 5 * (float(temp) - 273.15) + 32
            temp = round(temp,2)
            await ctx.send(str(format(oldtemp,",")) + " Kelvin is " + str(format(temp,",")) + "° Fahrenheit")
        else:
            await ctx.send("Invalid format")