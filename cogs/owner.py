from discord.ext import commands


class OwnerCog:

    def __init__(self, bot):
        self.bot = bot
    
    # Hidden means it won't show up on the default help.
    @commands.command(name='load', hidden=True)
    @commands.is_owner()
    async def cog_load(self, ctx, *, cog: str):
        """Command which Loads a Module.
        Remember to use dot path. e.g: cogs.owner"""

        try:
            self.bot.load_extension(cog)
        except Exception as e:
            await ctx.send('**ERRORI** {}'.format(e))
        else:
            print("Loaded Cog: " + cog)
            await ctx.send('**`SUCCESSFULLY LOADED {}`**'.format(cog))

    @commands.command(name='unload', hidden=True)
    @commands.is_owner()
    async def cog_unload(self, ctx, *, cog: str):
        """Command which Unloads a Module.
        Remember to use dot path. e.g: cogs.owner"""

        try:
            self.bot.unload_extension(cog)
        except Exception as e:
            await ctx.send('**`ERROR:`**{}'.format(e))
        else:
            print("Unloaded Cog: " + cog)
            await ctx.send('**`SUCCESSFULLY UNLOADED {}`**'.format(cog))

    @commands.command(name='reload', hidden=True)
    @commands.is_owner()
    async def cog_reload(self, ctx, *, cog: str):
        """Command which Reloads a Module.
        Remember to use dot path. e.g: cogs.owner"""

        try:
            self.bot.unload_extension(cog)
            self.bot.load_extension(cog)
        except Exception as e:
            await ctx.send('**`ERROR:`** {}'.format(e))
        else:
            print("Reloaded Cog: " + cog)
            await ctx.send('**`SUCCESSFULLY RELOADED {}`**'.format(cog))


def setup(bot):
    bot.add_cog(OwnerCog(bot))
