import discord
from discord.ext import commands
from PIL import Image, ImageDraw, ImageFont
from mods.textwp import text_wrap

def setup(bot):
    bot.add_cog(ClippyCOG(bot))


class ClippyCOG:

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def clippy(self, ctx, *,text: str):
        image = Image.open('images/clippy.png')
        image_size = image.size
        image_height = image.size[1]
        image_width = image.size[0]
        draw = ImageDraw.Draw(image)

        font = ImageFont.truetype('fonts/comic.ttf', size=20)
        #340 is the width we want to set the image width to
        lines = text_wrap(text, font, 340)
        line_height = font.getsize('hg')[1]
        (x, y) = (25, 20)
        color = 'rgb(0, 0, 0)' # black color
        text_size = draw.textsize(text, font=font)
        image_x = (image_width / 2) - (text_size[0] / 2)

        for line in lines:
            text_size = draw.textsize(line, font=font)
            image_x = (image_width /2 ) - (text_size[0]/2)
            draw.text((image_x, y), line, fill=color, font=font)
            y = y + line_height

        image.save('images/clippynow.png')
        await ctx.send(file=discord.File(fp='images/clippynow.png'))
