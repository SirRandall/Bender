import discord
from discord.ext import commands
from igdb_api_python.igdb import igdb
import time, os
from mods.platformids import Platform_Lookup
from mods.textwp import text_wrap
import random
from PIL import Image, ImageDraw, ImageFont

def setup(bot):
    bot.add_cog(Bender(bot))


class Bender:

    def __init__(self, bot):
        self.bot = bot
    
    @commands.command()
    async def game(self, ctx, *,game: str):
        #game = " ".join(str(p) for p in game)
        #counter = 0
        #while counter <= 20
        print("1."+game+" 78676876786")
        igdb1 = igdb("6745117656c7375b7d22d18d86cdfca4")
        result = igdb1.games({
            'search': "{}".format(game),
            'fields': ['name','game',
                'first_release_date','summary',
                'cover','platforms','url']
            })
        result = result.body
        sr=result[0]
        cover = sr['cover']
        platformconvert = sr['platforms']
        gameurl = sr['url']
        platformconvert.sort()
        plat = ''
        for i in platformconvert:
            plat += Platform_Lookup[i] + '\n'
        date = time.strftime("%a, %b %d %Y", time.localtime(sr['first_release_date']/1000.))
        summary = sr['summary'] if len(sr['summary']) <= 1024 else sr['summary'][:1021]+'...'
        embed=discord.Embed(title=sr['name'], url=gameurl, color=0xed)
        embed.add_field(name="Summary", value=summary, inline=False)
        embed.add_field(name="Release Date", value=date, inline=True)
        embed.add_field(name="Platforms", value=plat, inline=True)
        embed.set_thumbnail(url="http:{}".format(cover['url']))
        #    await ctx.send(sr['name'] + " was released on " + date)
        #[print(len(x.value)) for x in embed.fields]
        await ctx.send(embed=embed)
    
    @commands.command()
    async def bender(self, ctx):
        benderfile = [line.rstrip('\n') for line in open('bender.txt')]
        rndnum = random.randint(0, len(benderfile))
        image = Image.open('images/bender.jpg')
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype('fonts/Roboto-Bold.ttf', size=38)
        image_size = image.size
        color = 'rgb(255, 255, 255)' # black color
        shadowcolor = "black"
        lines = text_wrap(benderfile[rndnum], font, image_size[0])
        line_height = font.getsize('hg')[1]
        x = 10
        y = 20
        for line in lines:
            draw.text((x-2, y-2), line, fill=shadowcolor, font=font)
            draw.text((x+2, y-2), line, fill=shadowcolor, font=font)
            draw.text((x-2, y-2), line, fill=shadowcolor, font=font)
            draw.text((x+2, y+2), line, fill=shadowcolor, font=font)
            draw.text((x, y), line, fill=color, font=font)
            y = y + line_height
        image.save('images/bendersay.png', optimize=True)
        await ctx.send(file=discord.File(fp='images/bendersay.png'))
    
    @commands.command()
    async def m8ball(self, ctx):
        mballlines = [line.rstrip('\n') for line in open('ball.txt')]
        rndnum = random.randint(0, len(mballlines))
        await ctx.send(mballlines[rndnum])

    @commands.command()
    async def fc(self, ctx):
        fcfile = [line.rstrip('\n') for line in open('fc.txt')]
        rndnum = random.randint(0, len(fcfile))
        await ctx.send(fcfile[rndnum])

    @commands.command()
    async def ban(self, ctx, *user: str):
        banfile = [line.rstrip('\n') for line in open('ban.txt')]
        rndnum = random.randint(0, len(banfile))-1
        await ctx.send(banfile[rndnum] % user)
    
    @commands.command()
    async def addbender(self, ctx, *lines: str):
        linesc = ' '.join(lines)
        with open('bender.txt', 'a+') as f:
            f.write("{}\n".format(linesc))
            f.close()
        await ctx.send("Added: \"{}\" to bender".format(linesc))
    
    @commands.command()
    async def changestatus(self, ctx, *iname: str):
        str1 = " ".join(str(p) for p in iname)
        activity = discord.Game(str1)
        #   print(' '.join(str(p) for p in iname))
        await bot.change_presence(status=discord.Status.online, activity=activity)

    @commands.command()
    async def drink(self, ctx):
        await ctx.send(file=discord.File(fp='images/bender-booze.gif'))
    
    @commands.command()
    async def roll(self, ctx, dice: str):
        """Rolls a dice in NdN format."""
        try:
            rolls, limit = map(int, dice.split('d'))
        except Exception:
            await ctx.send('Format has to be in NdN!')
            return

        result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
        await ctx.send(result)
    
    @commands.command(description='For when you wanna settle the score some other way')
    async def choose(self, ctx, *choices: str):
        """Chooses between multiple choices."""
        await ctx.send(random.choice(choices))
    @commands.command()
    async def bestbandintheworld(self, ctx):
        await ctx.send("That would be U2")
    
