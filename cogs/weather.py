import discord
from discord.ext import commands
from mods.textwp import text_wrap
from weather import Weather, Unit
from PIL import Image, ImageDraw, ImageFont



def setup(bot):
    bot.add_cog(WeatherCog(bot))

class WeatherCog:
    """Weather Cog"""
     
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command()
    async def weathertext(self, ctx, TempUnit, *city: str):
        """Displays the weather from location provided in text form. You can use F or C for temprateure. Example !weather f death valley"""
        if TempUnit.lower() == "c":
            weather = Weather(unit=Unit.CELSIUS)
            unit = "C"
        elif TempUnit.lower() == "f":
            weather = Weather(unit=Unit.FAHRENHEIT)
            unit = "F"
        else: await ctx.send("Please use F or C for tempreature unit. Example !weather f las vegas")
        lookup = weather.lookup_by_location("{}".format(' '.join(city)))
        condition = lookup.condition
        forecast = lookup.forecast[0]
        await ctx.send("Today in " + lookup.location.city + lookup.location.region + " " + lookup.location.country + " it will be " + condition.text  + " with a high of " + forecast.high + "°" + unit + " with a low of " + forecast.low + "°" + unit + " current temp is " + condition.temp + "°" + unit)
    
    @commands.command()
    async def weather(self, ctx, TempUnit, *city: str):
        """Displays the weather from location provided. You can use F or C for temprateure. Example !weather f death valley"""
        if TempUnit.lower() == "c":
            weather = Weather(unit=Unit.CELSIUS)
            unit = "C"
        elif TempUnit.lower() == "k":
            weather = Weather(unit=Unit.FAHRENHEIT)
            unit = "K"
        elif TempUnit.lower() == "f":
            weather = Weather(unit=Unit.FAHRENHEIT)
            unit = "F"
        else: await ctx.send("Please use F or C for tempreature unit. Example !weather f las vegas")
        lookup = weather.lookup_by_location('{}'.format(' '.join(city)))
        condition = lookup.condition
        forecast = lookup.forecast[0]
        #load the fonts
        font = ImageFont.truetype('fonts/Roboto-Bold.ttf', size=70)
        font2 = ImageFont.truetype('fonts/Roboto-Thin.ttf', size=40)
        tempfont = ImageFont.truetype('fonts/Roboto-Bold.ttf', size=130)
        ktempfont = ImageFont.truetype('fonts/Roboto-Bold.ttf', size=100)
        hilowfont = ImageFont.truetype('fonts/Roboto-Thin.ttf', size = 80)
        color = 'rgb(255, 255, 255)' # black color
        #check weather for image
        if condition.text == "Mostly Sunny" or condition.text == "Sunny":
            image = Image.open('images/sunny.png')
        elif condition.text =="Partly Cloudy":
            image = Image.open('images/partcloudy.png')
        elif condition.text =="Cloudy" or condition.text == "Mostly Cloudy":
            image = Image.open('images/cloudy.png')
        elif condition.text =="Windy" or condition.text == "Breezy":
            image = Image.open('images/windy.png')
        elif condition.text == "Showers":
            image = Image.open('images/rain.png')
        else:
            image = Image.open('images/template.png')
        draw = ImageDraw.Draw(image)
        #get image size                
        image_size = image.size
        image_height = image.size[1]
        image_width = image.size[0]
        #conk = int(condition.temp) + 273.15
        conk = (float(condition.temp) + 459.67) * 5 / 9
        conk = round(conk, 2)
        #print(conk)
        #check temp for formatting
        if int(condition.temp) <= 99 and unit !="K":
            (x, y) = (950, 320)
            draw.text((x, y), condition.temp + "°" + unit.upper(), fill=color, font=tempfont)
        else:
            (x, y) = (945, 320)
            if unit != "K":
                draw.text((x, y), condition.temp + "°" + unit.upper(), fill=color, font=tempfont)
            else:
                draw.text((x, y), str(conk) +  unit.upper(), fill=color, font=ktempfont)
        #starting of text
        (x, y) = (50, 50)
        color = 'rgb(255, 255, 255)' # black color
        # City + State
        draw.text((x, y), lookup.location.city.upper() + lookup.location.region.upper(), fill=color, font=font)
        # County
        (x, y) = (50, 130)
        draw.text((x, y), lookup.location.country.upper(), fill=color, font=font)
        #Hight temp
        #(x, y) = (980,100)
        if unit != "K":
            (x, y) = (980,100)
            draw.text((x, y), "High: " + forecast.high + "°", fill=color, font=hilowfont)
        else:
            (x, y) = (880,100)
            #fhigh = int(forecast.high) + 273.15
            fhigh = (float(forecast.high) + 459.67) * 5 / 9
            fhigh = round(fhigh, 2)
            draw.text((x, y), "High: " + str(fhigh) + "K", fill=color, font=hilowfont)
        #(x, y) = (980,200i)
        if unit != "K":
            (x, y) = (980,200)
            draw.text((x, y), "Low: " + forecast.low + "°", fill=color, font=hilowfont)
        else:
          (x, y) = (880,200)
          #flow = int(forecast.low) + 273.15
          flow = (float(forecast.low) + 459.67) * 5 / 9
          flow = round(flow, 2)
          draw.text((x, y), "Low: " + str(flow) + "K", fill=color, font=hilowfont)
         #this is for centering the condidtion text center
        text_size = draw.textsize(condition.text, font=font)
        image_x = (image_width / 2) - (text_size[0] / 2)
        draw.text((image_x, 600), condition.text, fill=color, font=font)
        #save file
        image.save('images/weathernow.png')
        #await ctx.send("Today will be " + condition.text  + " with a high of " + forecast.high + "°" + unit + " with a low of " + forecast.low + "°" + unit + " current temp is " + condition.temp + "°" + unit)
        await ctx.send(file=discord.File(fp='images/weathernow.png'))
    
    ''''@weather.error
    async def weather_error(self, ctx, error):
        #if isinstance(error, CheckFailure):
        msg = 'Error: {} Example !weather f death valley'.format(error)
        await ctx.channel.send(msg)'''
