import discord
from discord.ext import commands
from mods.morse1 import encrypt, decrypt

def setup(bot):
    bot.add_cog(Morse(bot))

class Morse:
    """Morse Code Cog"""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def morse(self, ctx, deen, *, message: str):
        """Converts Text <-> Morse1"""
        #message1 = " ".join(str(p) for p in message)
        message1 = message
        if deen.lower() == "en" or deen.lower() == "encode":
            message = encrypt(message1.upper())
        if deen.lower() == "de" or deen.lower() == "decode":
            message = decrypt(message1)
        await ctx.send(message)
