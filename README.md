**Basic Discord Bot**
# Basic Install Guide
I have used several modules here are the ones that this bot uses.

You need to make a file called token.txt and put your Discord token in that file.
You can get your token here https://discordapp.com/developers/

You need to have the following modules installed 
* https://github.com/Rapptz/discord.py This is the Discord Python API

we need to install the rewrite version. To do that run this pip install command. 
```
pip install -U git+https://github.com/Rapptz/discord.py@rewrite
```

* https://github.com/AnthonyBloomer/weather-api yahoo python weather API this is for getting weather
* https://github.com/igdb/igdb_api_python This is IGDB Python API this is for getting imformation on video games 
* https://github.com/python-pillow/Pillow This is used for weather and bender. It's used to generate the images.


# Goals

* I want this to be a full featured bot for discord along with admin/mod tools 
* I would like to add IMDB and some other look up tools. 
* Clean up the code and make it more professinal right now it's kind of a mess.
*