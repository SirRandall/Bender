#!/usr/bin/python3
import discord
from discord import Game
from discord.ext import commands
from discord.ext.commands import Bot, has_permissions, CheckFailure
import asyncio
import aiohttp
import datetime
import time, os
from os import listdir
from os.path import isfile, join
import sys, traceback

description = '''Bender Bot. Basic Discord bot.'''
BOT_PREFIX = ("?", "!")
bot = commands.Bot(command_prefix=BOT_PREFIX, description=description)
client = discord.Client()

cogs_dir = "cogs"

if __name__ == '__main__':
    for extension in [f.replace('.py', '') for f in listdir(cogs_dir) if isfile(join(cogs_dir, f))]:
        try:
            bot.load_extension(cogs_dir + "." + extension)
            print("Loaded Cog:" + extension)
        #except (discord.ClientException, ModuleNotFoundError):
        except Exception as e:
            print('Failed to load extension {}.'.format(extension))
            traceback.print_exc()

with open('token.txt', 'r') as f:
        token = f.read().strip()
            
@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    print("Current servers:")
    for i in bot.guilds:
        print(i)
    activity = discord.Game("Bending")
    await bot.change_presence(status=discord.Status.online, activity=activity)

bot.run(token)

